package mosby.mvp.test.daggerUtil.component;

import com.squareup.picasso.Picasso;

import dagger.Component;
import mosby.mvp.test.api.ApiService;
import mosby.mvp.test.daggerUtil.module.ApiServiceModule;
import mosby.mvp.test.daggerUtil.module.PicassoModule;
import mosby.mvp.test.daggerUtil.qualifier.ApiServiceQualifier;
import mosby.mvp.test.daggerUtil.scopes.DaggerTutorialApplicationScope;


/**
 * Created by Taher on 21/04/2017.
 * Project: DaggerTutorial
 */

@DaggerTutorialApplicationScope
@Component(modules = {ApiServiceModule.class, PicassoModule.class})
public interface DaggerTutorialApplicationComponent {

    Picasso getPicasso();

    @ApiServiceQualifier
    ApiService getApiService();
}

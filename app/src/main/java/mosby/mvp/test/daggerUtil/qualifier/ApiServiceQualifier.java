package mosby.mvp.test.daggerUtil.qualifier;

import javax.inject.Qualifier;

/**
 * Created by Taher on 21/04/2017.
 * Project: DaggerTutorial
 */
@Qualifier
public @interface ApiServiceQualifier {
}

package mosby.mvp.test.daggerUtil.component;

import javax.inject.Singleton;

import dagger.Component;
import mosby.mvp.test.daggerUtil.module.UserModule;
import mosby.mvp.test.model.User;

@Singleton
@Component(modules = {UserModule.class})
//@Component(modules = {Module1.class,Mudule2.class})
public interface UserComponent {
    User provideUser();
}